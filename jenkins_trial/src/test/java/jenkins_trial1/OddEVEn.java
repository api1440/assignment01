package jenkins_trial1;
import jenkins_trial.*;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class OddEVEn {
	static evenOdd obj;
	
	@Test
	public void testcase0() {
		 obj = new evenOdd();
		assertTrue(obj.EvenOdd(2));
		System.out.println("test case 1 executed");
	}
	@Test
	public void testcase1() {
		assertFalse(obj.EvenOdd(19));
		System.out.println("test case 2 executed");
	}
	@Test
	public void testcase2() {
		assertNull(obj.EvenOdd(15));
		System.out.println("test case 3 executed");
	}
	@Test
	public void testcase3() {
		assertNotNull(obj.EvenOdd(0));
		System.out.println("test case 4 executed");
	}
	@BeforeEach
	public void before() {
		System.out.println("Executing this before each ");
	}
	@AfterEach
	public void after() {
		System.out.println("Executing this after each ");
	}
}
